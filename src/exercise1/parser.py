from abc import ABC
from numpy import double
from abc import ABC,abstractmethod
import re

class Expression(ABC):
    @abstractmethod
    def calc(self)->double:
        pass

# implement the classes here
class Num(Expression):
    def __init__(self, num: double):
        self.num = num

    def calc(self)->double:
        return self.num
    
    @staticmethod
    def is_num(value: str):
        try:
            float(value)
            return True
        except ValueError:
            return False
    

class BinExp(Expression):
    def __init__(self, left: Expression, right: Expression):
        self.left = left
        self.right = right


class Plus(BinExp):
    def __init__(self, left: Expression, right: Expression) -> None:
        super().__init__(left, right)

    def calc(self)->double:
        return self.left.calc() + self.right.calc()


class Minus(BinExp):
    def __init__(self, left: Expression, right: Expression) -> None:
        super().__init__(left, right)

    def calc(self)->double:
        return self.left.calc() - self.right.calc()


class Mul(BinExp):
    def __init__(self, left: Expression, right: Expression) -> None:
        super().__init__(left, right)

    def calc(self)->double:
        return self.left.calc() * self.right.calc()


class Div(BinExp):
    def __init__(self, left: Expression, right: Expression) -> None:
        super().__init__(left, right)

    def calc(self)->double:
        return self.left.calc() / self.right.calc()

#implement the parser function here

def get_expression_tokens(expression) -> list:
     expression = expression.replace('(-', '(0-')
     return re.split("(?<=[-+*/()])|(?=[-+*/()])", expression)


def get_posfix(expression_tokens: list) -> list:
    s = []
    q = []
    for token in expression_tokens:
        if Num.is_num(token):
            q.append(token)
        else:
            if token == '/' or token == '*' or token == '(':
                s.append(token)
            elif token == '+' or token == '-':
                while s and s[-1] != '(':
                    q.append(s.pop())
                s.append(token)
            elif token == ')':
                while s[-1] != '(':
                    q.append(s.pop())
                s.pop()

    while s:
        q.append(s.pop())
    
    return q


def parser(expression)->double:
    expStack = []
   
    expression_tokens = get_expression_tokens(expression)
    posfix_queue = get_posfix(expression_tokens)
    
    for token in posfix_queue:
        if Num.is_num(token):
            expStack.append(Num(float(token)))
        else:
            right = expStack.pop()
            left = expStack.pop()
            if token == '/':
                expStack.append(Div(left, right))
            elif token == '*':
                expStack.append(Mul(left, right))
            elif token == '+':
                expStack.append(Plus(left, right))
            elif token == '-':
                expStack.append(Minus(left, right))

    result = expStack.pop().calc()
    return result