

def dnc(baseFunc, combineFunc):
    def my_func(arr):
        if len(arr) == 1:
            return baseFunc(arr[0])
        else:
            left = my_func(arr[:len(arr) // 2])
            right = my_func(arr[len(arr) // 2:])
            return combineFunc(left, right)
    return my_func


def getArea(stack, data, index):
    top = stack.pop()
    height = data[top]

    if stack:
        width = index - stack[-1] - 1
    else:
        width = index

    return height * width
    

def maxAreaHist(data):
    stack = []
    max_area = 0
    index = 0
    while index < len(data):
        if (not stack) or (data[stack[-1]] <= data[index]):
            stack.append(index)
            index += 1
        else:
            max_area = max(max_area, getArea(stack, data, index))

    while stack:
        max_area = max(max_area, getArea(stack, data, index))

    return max_area
