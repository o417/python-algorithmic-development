import threading
from queue import Queue

class Stream():
 
    stopItem = None # When a stream has this item in his list, then its need to stop

    def __init__(self):
        self.queue = Queue()
        self.function = None
        self.thread = threading.Thread(target=self.run)
        self.next_stream = None
        self.thread.start()


    def add(self, item):
        self.queue.put(item)
        

    def forEach(self, function):
        self.function = function


    def apply(self, function):
        self.function = function
        self.next_stream = Stream()
        return self.next_stream


    def run(self):
        while True:
            item = self.queue.get()  # blocks until an item is available
            if item is Stream.stopItem:
                break
            if self.function:
                result = self.function(item)
                if self.next_stream:
                    if result:
                        if isinstance(result, bool): # forEach() function
                            self.next_stream.add(item)
                        else: # apply() function
                            self.next_stream.add(result)


    def stop(self):
        self.add(Stream.stopItem) # When the stream arrive to this item in his list, the stream will stop
        self.thread.join() # stop the stream's thread
        if self.next_stream:
            self.next_stream.stop() # stop the next stream
