import math
import random
import numpy as np

class Point:
    def __init__(self) -> None:
        self.x = random.randint(-100, 100)
        self.y = random.randint(-100, 100)
    
def dist(p1, p2):
    return float(math.sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2))


# This function calculates the total distance for a round trip
def fitness(points):

    # This sum corresponds to the squared Euclidean distance between each pair of points.
    # The sqrt give the actual Euclidean distance between each pair of points
    x_diff_matrix = np.subtract.outer([p.x for p in points], [p.x for p in points])
    y_diff_matrix = np.subtract.outer([p.y for p in points], [p.y for p in points])
    
    # dist_matrix - each element [i, j] represents the distance from point i to point j
    dist_matrix = np.sqrt(x_diff_matrix ** 2 +  y_diff_matrix ** 2)

    # Calculate the total distance
    row_points_indexes = np.arange(len(points))
    col_points_indexes = np.roll(row_points_indexes, -1)
    # The distance from point (0, 1) to (1,2) to (i, i+1) to (n-1, n)
    return np.sum(dist_matrix[row_points_indexes, col_points_indexes])


def create_population(points, pop_size):
    population = []
    for i in range(pop_size):
        individual = points.copy() # individual is a possible solution
        random.shuffle(individual) # the order of the points represent the route
        population.append(individual)
    return population



def crossover(parent1, parent2):
    child = [None] * len(parent1)
    parent1_gens_start = random.randint(0, len(parent1) - 1)
    parent1_gens_end = random.randint(parent1_gens_start, len(parent1) - 1)

    for i in range(parent1_gens_start, parent1_gens_end + 1):
        child[i] = parent1[i]

    idx = 0
    # the lefts gens will be from parent2
    for i in range(len(parent2)):
        if parent2[i] not in child:
            while child[idx] is not None:
                idx += 1
            child[idx] = parent2[i]
    return child


def mutate(individual, mutation_rate):
    for i in range(len(individual)):
        if random.random() < mutation_rate:
            j = random.randint(0, len(individual) - 1)
            # switch 2 cells
            individual[i], individual[j] = individual[j], individual[i]


def select_parents(population, fitnesses):
    # Perform roulette wheel selection
    total_fitness = sum(fitnesses)
    selection_probs = [f / total_fitness for f in fitnesses]
    parent1_idx = np.random.choice(len(population), p=selection_probs)
    parent2_idx = np.random.choice(len(population), p=selection_probs)

    while parent2_idx == parent1_idx:
        parent2_idx = np.random.choice(len(population), p=selection_probs)

    return population[parent1_idx], population[parent2_idx]


def solve(points, pop_size=100, elite_size=10, mutation_rate=0.01, generations=100):
    population = create_population(points, pop_size)
    best_distance = float('inf')
    best_individual = None

    for generation in range(generations):
        fitnesses = [fitness(individual) for individual in population]
        fitnesses_asc_values_indexes = np.argsort(fitnesses)
        fitnesses_lowest_indexes = fitnesses_asc_values_indexes[:elite_size]
        elites = [population[idx] for idx in fitnesses_lowest_indexes ]

        if fitnesses[0] < best_distance:
            best_distance = fitnesses[0]
            best_individual = population[0]
        next_gen = elites.copy()

        while len(next_gen) < pop_size:
            parent1, parent2 = select_parents(population, fitnesses)
            child = crossover(parent1, parent2)
            mutate(child, mutation_rate)
            next_gen.append(child)

        population = next_gen

    return best_individual
