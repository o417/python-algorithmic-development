def allSumsDP(arr):
    n = len(arr)
    dp = [set() for _ in range(n+1)]
    dp[0].add(0)

    for i in range(1, n+1):
        for j in dp[i-1]:
            dp[i].add(j)
            dp[i].add(j+arr[i-1])
    return dp[n]
